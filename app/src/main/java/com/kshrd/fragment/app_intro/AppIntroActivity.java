package com.kshrd.fragment.app_intro;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.kshrd.fragment.R;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        addSlide(AppIntroFragment.newInstance(
//                "Page 1", "Description 1", R.drawable.facebook, ContextCompat.getColor(this, R.color.colorAccent)
//        ));
//
//        addSlide(AppIntroFragment.newInstance(
//                "Page 2", "Description 2", R.drawable.facebook, ContextCompat.getColor(this, R.color.colorAccent)
//        ));

        addSlide(PageFragment.newInstance("Page 1"));
        addSlide(PageFragment.newInstance("Page 2"));

        setZoomAnimation();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
