package com.kshrd.fragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kshrd.fragment.fragment.ArticleDetailFragment;
import com.kshrd.fragment.fragment.ArticleFragment;

public class StaticFragmentActivity extends AppCompatActivity implements ArticleFragment.ArticleFragmentListener {

    ArticleFragment articleFragment;
    ArticleDetailFragment articleDetailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        articleFragment = (ArticleFragment) getSupportFragmentManager().findFragmentById(R.id.articleFragment);
        articleDetailFragment = (ArticleDetailFragment) getSupportFragmentManager().findFragmentById(R.id.articleDetailFragment);

    }

    @Override
    public void onChangeDescriptionClicked(String str) {
        articleDetailFragment.updateDescription(str);
    }
}
