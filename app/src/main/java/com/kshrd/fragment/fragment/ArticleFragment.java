package com.kshrd.fragment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.kshrd.fragment.R;

/**
 * Created by pirang on 7/8/17.
 */

public class ArticleFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ArticleFragmentListener listener;
    private ListView lvArticle;
    private String[] brands;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ArticleFragmentListener) {
            listener = (ArticleFragmentListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        lvArticle = (ListView) view.findViewById(R.id.lvArticle);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        brands = new String[]{"Honda", "Suzuki", "KTM", "Yamaha", "Toyota", "Mazda"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_list_item_1, brands
        );
        lvArticle.setAdapter(adapter);
        lvArticle.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (listener != null) {
            listener.onChangeDescriptionClicked(brands[position]);
        }
    }

    public interface ArticleFragmentListener {
        void onChangeDescriptionClicked(String str);
    }
}
