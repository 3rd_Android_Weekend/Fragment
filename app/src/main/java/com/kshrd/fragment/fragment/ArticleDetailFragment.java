package com.kshrd.fragment.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kshrd.fragment.R;

public class ArticleDetailFragment extends Fragment {

    TextView tvDescription;

    public ArticleDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_article_detail, container, false);
        tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        return view;
    }

    public void updateDescription(String str){
        tvDescription.setText(str);
    }

}
