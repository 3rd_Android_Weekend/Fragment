package com.kshrd.fragment.tab.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by pirang on 7/8/17.
 */

public class Tab {

    private String title;
    private int icon;
    private Fragment fragment;

    public Tab(String title, int icon, Fragment fragment) {
        this.title = title;
        this.icon = icon;
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }
}
