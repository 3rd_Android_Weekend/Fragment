package com.kshrd.fragment.tab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kshrd.fragment.tab.fragment.FavoriteFragment;
import com.kshrd.fragment.tab.fragment.HomeFragment;

/**
 * Created by pirang on 7/8/17.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = HomeFragment.newInstance();
                break;
            case 1:
                fragment = FavoriteFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "N/A";
        switch (position) {
            case 0:
                title = "Home";
                break;
            case 1:
                title = "Favorite";
                break;
        }
        return title;
    }


}
