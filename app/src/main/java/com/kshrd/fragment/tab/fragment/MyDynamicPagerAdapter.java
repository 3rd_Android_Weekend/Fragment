package com.kshrd.fragment.tab.fragment;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kshrd.fragment.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 7/8/17.
 */

public class MyDynamicPagerAdapter extends FragmentPagerAdapter {

    private List<Tab> tabList;

    public MyDynamicPagerAdapter(FragmentManager fm) {
        super(fm);
        tabList = new ArrayList<>();
    }

    public void addTab(Tab tab) {
        tabList.add(tab);
    }

    @Override
    public int getCount() {
        return tabList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return tabList.get(position).getFragment();
    }

    public void setTabIcon(TabLayout tabLayout) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setIcon(tabList.get(i).getIcon());
        }
    }

    public void setCustomTabView(TabLayout tabLayout) {

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setCustomView(getCustomView(i, tabLayout.getContext()));
        }

    }

    private View getCustomView(int position, Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);

        Tab tab = tabList.get(position);
        tvTitle.setText(tab.getTitle());
        ivIcon.setImageResource(tab.getIcon());

        return view;
    }
}
