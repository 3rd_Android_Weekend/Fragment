package com.kshrd.fragment.tab;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kshrd.fragment.R;
import com.kshrd.fragment.tab.fragment.FavoriteFragment;
import com.kshrd.fragment.tab.fragment.HomeFragment;
import com.kshrd.fragment.tab.fragment.MyDynamicPagerAdapter;
import com.kshrd.fragment.tab.fragment.Tab;

public class DynamicTabActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_tab);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        MyDynamicPagerAdapter adapter = new MyDynamicPagerAdapter(getSupportFragmentManager());
        adapter.addTab(new Tab("Home", R.drawable.home_selector, HomeFragment.newInstance()));
        adapter.addTab(new Tab("Favorite", R.drawable.favorite_selector, FavoriteFragment.newInstance()));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        adapter.setCustomTabView(tabLayout);
    }
}
